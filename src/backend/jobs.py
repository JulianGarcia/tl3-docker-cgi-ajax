#!/usr/bin/python3
import os
import cgi
import cgitb
import json
import sqlalchemy
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import logging
import datetime

from db_handler import Database

logger = logging.getLogger()
cgitb.enable()

print("Content-Type: application/json;charset=utf-8")
print()

db_string = "postgres://admin:admin@tl3_db:5432/tl3"
engine = create_engine(db_string, echo=False)

metadata = MetaData()

metadata.create_all(engine) 
Session = sessionmaker(bind=engine)
session = Session()


class Job(declarative_base()):
    __tablename__ = 'jobs'

    id = Column(Integer, primary_key=True)
    job_name = Column(String)
    ocupation = Column(String)
    date_in = Column(DateTime)
    date_off = Column(DateTime)
    user_id = Column(Integer)

    def __init__(self, job_name, ocupation, date_in, date_off, user_id):
        self.job_name= job_name
        self.ocupation = ocupation
        self.date_in = date_in
        self.date_off = date_off
        self.user_id = user_id


def query_jobs():
    jobs = []
    for j in session.query(Job).all():
        job = j.__dict__
        job.pop('_sa_instance_state', None)    
        jobs.append(job)
    logger.error(jobs)
    return jobs


def create_job():
    try:
        logger.exception("estoy en create job")
        form = cgi.FieldStorage()
        logger.exception(form)
        job = Job(
            form.getvalue('job_name'),
            form.getvalue('ocupation'),
            form.getvalue('date_in'),
            form.getvalue('date_off'),
            1
            )
        logger.exception("este es el trabajo")
        logger.exception(job)
        session.add(job)
        session.commit()
        return {'error': False}
    except:
        return {'error': True}


if os.environ['REQUEST_METHOD'] == 'GET':
    response = query_jobs()    
if os.environ['REQUEST_METHOD'] == 'POST':
    logger.exception("vine por post")
    response = create_job()
if not response:
    response = {}


print(json.JSONEncoder().encode(response))

